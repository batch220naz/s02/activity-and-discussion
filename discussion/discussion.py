# [SECTION] Input
# input() is similar to prompt() in JS that seeks to gather data from user input. it returns string data type.
# "\n" stands for line break

"""
username = input("Please enter your name:\n")
print(f"Hi {username}! Welcome to Python Short Course!")

num1 = int(input("Please enter the first number:\n"))
num2 = int(input("Please enter the second number:\n"))
print(f"The sum of num1 and num2 is {num1 + num2}")
"""

# [SECTION] If-Else Statement
test_num = 5
# in Python, the condition that the if statement gas to assess is not enclosed in parenthesis. at the end of the statement, there is a colon, denoting that indented statements below will be executed

# the rule for indention is also applicable for other code syntax such as functions
# if test_num >= 60  and test_num == 0 : - if we need to assess multiple conditions, use logical operators

"""
if test_num >= 60 :
	print("Test Passed")
else :
	print("Test Failed")

# if-else chains
test_num2 = int(input("Please enter a number for testing:\n"))
if test_num2 > 0 :
	print("The number is positive")
# elif is the equal of "else if" keyword in JS
elif test_num2 == 0 :
	print("The number is zero")
else :
	print("The number is negative")
"""

# [Mini Activity] 

number = int(input("Please enter a number for mini activity:\n"))

if number % 3 == 0 and number % 5 == 0:
	print("The number is divisible by 3 and 5")
elif number % 3 == 0 :
	print("The number is divisible by 3")
elif number % 5 == 0 :
	print("The number is divisible by 5")
else :
	print("The number is not divisible by 3 nor 5")




# [SECTION] Loops
# [SECTION] While Loops
# Performs a code block as long as the condition is true
i = 1
while i <= 5 :
	print(f"Current Value: {i}")
	i+=1





# [SECTION] For Loop
# https://www.w3schools.com/python/python_for_loops.asp
# used to iterate through values or sequence
fruits = ["apple", "orange",  "banana"]

for indiv_fruit in fruits :
	print(indiv_fruit)

# 
# range() method returns the sequence of the given number
# using range() method would allow us to iterate through values
# https://www.geeksforgeeks.org/python-range-function/
"""
SYNTAX
	range(stop) 
	range(start, stop)
	range(start, stop, step)
"""

range_value1 = range(6)
range_value2 = range(1, 10)
range_value3 = range(1, 10, 2)
print(range_value1)
print(range_value2)
print(range_value3)

for x in range_value1 :
	print(f"Current Value Range 1: {x}")
print("")

for x in range_value2 :
	print(f"Current Value Range 2: {x}")
print("")

for x in range_value3 :
	print(f"Current Value Range 3: {x}")
print("")






# [SECTION] Break and Continue Statement
# Break statement
# break is used to stop the loop
j = 1
while j < 6 :
	print(j)
	if j == 3 :
		break
	j += 1
print("")

# Continue Statement
# continue statement is used to stop the current iteration and continue to the next iteration
# takes the control back to the top of the iteration
k = 1
while k < 6 :
	k += 1 # signifies the top of the loop should we use continue statement
	if k == 3 :
		continue
	print(k)